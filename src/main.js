import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createHead } from '@unhead/vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'

import App from './App.vue'
import router from './router'

const app = createApp(App)

const head = createHead()

app.use(createPinia())
app.use(head)
app.use(router)

app.mount('#app')
